import React, { useState } from "react";
import Counter from "./components/Counter";

import "./App.css";

const counterData = [
  { id: 0, value: 0 },
  { id: 1, value: 0 },
  { id: 2, value: 0 },
  { id: 3, value: 0 },
];

function App() {
  const [data, setData] = useState(counterData);

  const increaseCounter = (counterId) => {
    const updatedData = data.map((each) => {
      if (each.id === counterId) {
        return { id:counterId, value: each.value + 1 };
      }else{
        return each;
      }
    });
    setData(updatedData);
  };

  const decreaseCounter = (counterId) => {
    const updatedData = data.map((each) => {
      if (each.id === counterId) {
        return { id: counterId, value: each.value > 0 ? each.value - 1 : 0 };
      }else{
        return each;
      }
      
    });
    setData(updatedData);
  };
  
  const onDelete = (counterId) => {
    const newData = data.filter((each) => {
      return each.id !== counterId;
    });
    setData(newData);
  };

 

  const onReset = () => {
    const resetData = data.map((each) => {
      each.value = 0;
      return each;
    });
    setData(resetData);
  };

  const restart = () => {
    if (data.length===0){
      window.location.reload();
    }
  };

  const cartCountdata=()=>{
    const newData = data.filter((each)=>{
      return each.value > 0
    })
    if(newData.length ===0){
      return 0;
    }else{
      return newData.length
    }
}
  
  return (
    <div className="main-container">
       <div className="main-cart-container">
        <i className="fa-solid fa-cart-shopping fa-lg"></i>
        <p className="cart">{cartCountdata()}</p>
        <p className="cart-desc">Items</p>
      </div>
      <div>
        <button onClick={onReset} className="reset-btn">
        <i class="fa-solid fa-arrows-rotate fa-lg"></i>
        </button>
        <button onClick={restart} className="restart-btn">
        <i class="fa-solid fa-recycle fa-lg"></i>
        </button>
      </div>
      <div className="counter-container">
        {data.map((each) => (
          <Counter
            key={each.id}
            counterId={each.id}
            value={each.value}
            onDelete={onDelete}
            increaseCounter={increaseCounter}
            decreaseCounter={decreaseCounter}
          />
        ))}
      </div>
    </div>
  );
}

export default App;
