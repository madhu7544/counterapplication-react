import React from 'react';

const Counter = (props) => {
  const { value, onDelete, counterId, increaseCounter, decreaseCounter } = props;
  
  const deleteBtn = () => {
    onDelete(counterId);
  };

  const increase = () => {
    increaseCounter(counterId);
  };

  const decrease = () => {
    decreaseCounter(counterId);
  };

  return (
    <div className="counter-app">
      <p className={value > 0 ? "span-counter-more-zero" : "span-counter"}>
        {value === 0 ? "Zero" : value}
      </p>
      <button onClick={increase} className="increase-btn">
        +
      </button>
      <button onClick={decrease} className="decrease-btn">
        -
      </button>
      <button className="delete-btn" onClick={deleteBtn}>
      <i class="fa-solid fa-trash-can fa-lg"></i>
      </button>
    </div>
  );
};

export default Counter;









































































// import { useState } from 'react';

// const Counter =(props)=>{
//     let {value,onDelete,counterId} = props;
//     const [counter,setCount] = useState(value);

//     const deleteBtn=()=>{
//         onDelete(counterId)
//     }

//     const increase=()=>{
//         setCount(counter+1)
//     }

//     const decrease=()=>{
//         if (counter===0){
//             setCount(0)
//         }
//         else{
//             setCount(counter-1)
//         }
//     }

//     return (
//         <div className='counter-app'>
//             <p className={counter>0 ? "span-counter-more-zero":"span-counter"} >{counter===0 ? "Zero":counter}</p>
//             <button onClick={increase} className='increase-btn'>+</button>
//             <button onClick={decrease} className='decrease-btn'>-</button>
//             <button className='delete-btn' onClick={deleteBtn}>Delete</button>
//         </div>
//     )
// }

// export default Counter;